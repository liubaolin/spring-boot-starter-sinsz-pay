package com.sinsz.pay.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 查询订单返回数据
 * @author chenjianbo
 */
public class OrderQueryDto {

    /**
     * 交易状态
     */
    private String tradeState;

    /**
     * 订单总金额
     */
    private String totalFee;

    /**
     * 支付平台订单号
     */
    private String transactionId;

    /**
     * 自定义商户订单号
     */
    private String outTradeNo;

    /**
     * 支付完成时间
     */
    private String timeEnd;

    /**
     * 交易状态描述
     */
    private String tradeStateDesc;

    public OrderQueryDto() {
    }

    public OrderQueryDto(String tradeState, String totalFee, String transactionId,
                         String outTradeNo, String timeEnd,
                         String tradeStateDesc) {
        this.tradeState = tradeState;
        this.totalFee = totalFee;
        this.transactionId = transactionId;
        this.outTradeNo = outTradeNo;
        this.timeEnd = timeEnd;
        this.tradeStateDesc = tradeStateDesc;
    }

    public String getTradeState() {
        return tradeState;
    }

    public void setTradeState(String tradeState) {
        this.tradeState = tradeState;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTradeStateDesc() {
        return tradeStateDesc;
    }

    public void setTradeStateDesc(String tradeStateDesc) {
        this.tradeStateDesc = tradeStateDesc;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return super.toString();
    }
}
