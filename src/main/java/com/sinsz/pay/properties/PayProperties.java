package com.sinsz.pay.properties;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

/**
 * 支付配置
 * @author chenjianbo
 */
@Validated
@ConfigurationProperties("sinsz.pay")
public class PayProperties {

    /**
     * 默认的接口访问域名地址，支持https、http
     */
    @NotEmpty
    private String http;

    /**
     * 微信支付配置
     */
    private Wxpay wxpay;

    /**
     * 支付宝支付配置
     */
    private Alipay alipay;

    public String getHttp() {
        return http;
    }

    public void setHttp(String http) {
        this.http = http;
    }

    public Wxpay getWxpay() {
        return wxpay;
    }

    public void setWxpay(Wxpay wxpay) {
        this.wxpay = wxpay;
    }

    public Alipay getAlipay() {
        return alipay;
    }

    public void setAlipay(Alipay alipay) {
        this.alipay = alipay;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace(System.out);
        }
        return super.toString();
    }

}
